import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { TokenService } from "../security/token.service";
import { AppConstants } from "../../app.constants";
import { AuthService } from "../security/auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  urlsWithoutToken: string [];

  constructor(private authService: AuthService, private tokenService: TokenService) {
    this.urlsWithoutToken = [AppConstants.OAUTH_TOKEN_API_URL];
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return !this.urlsWithoutToken.includes(request.url)
      ? from(this.handleRequestWithToken(request, next))
      : next.handle(request);
  }

  async handleRequestWithToken(request: HttpRequest<unknown>, next: HttpHandler) {
    if (!this.tokenService.isTokenValid()) {
      const createdToken = await this.authService.getToken().toPromise();
      this.tokenService.saveToken(createdToken);
    }
    const requestWithToken = request.clone({
      headers: request.headers.set('Authorization', `Bearer ${this.tokenService.getToken()}`),
    });
    return next.handle(requestWithToken).toPromise();
  }
}
