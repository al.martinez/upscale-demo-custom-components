import { Injectable } from '@angular/core';
import { Token } from "../../data/models/token.model";

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  private token!: Token;

  getToken(): string {
    return this.token?.getAccessToken;
  }

  isTokenValid(): boolean {
    return this.token?.getExpiresIn > 0
  }

  saveToken(tokenData: any): void {
    this.token = new Token(tokenData?.access_token, tokenData?.expires_in);
  }
}
