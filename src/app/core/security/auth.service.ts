import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AppConstants } from "../../app.constants";
import { Observable } from "rxjs";

const HTTP_HEADERS = new HttpHeaders()
  .set('Content-Type', 'application/x-www-form-urlencoded');

const HTTP_BODY = new HttpParams()
  .set('client_id', AppConstants.CLIENT_ID)
  .set('client_secret', AppConstants.CLIENT_SECRET)
  .set('grant_type', AppConstants.GRANT_TYPE)

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) {
  }

  getToken(): Observable<any> {
    return this.httpClient.post<any>(AppConstants.OAUTH_TOKEN_API_URL, HTTP_BODY, { headers: HTTP_HEADERS });
  }
}
