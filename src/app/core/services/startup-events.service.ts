import { Injectable } from '@angular/core';
import { Event } from '../../data/models/event.model';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StartupEventsService {

  sendStartupEvents(height = 0): void {
    StartupEventsService.sendEvent(new Event('initialized', null));
    StartupEventsService.sendEvent(new Event('sizeChange', { height }));
  }

  private static sendEvent(event: any, origin = environment.storefrontUrl): void {
    const standardWindow = (window as any);
    const webkit = standardWindow.webkit;

    if (standardWindow.parent !== standardWindow) {
      // Web
      standardWindow.parent.postMessage(event, origin);
    } else if (standardWindow.Android) {
      // Android
      standardWindow.parent.postMessage(event, origin);
    } else if (webkit && webkit.messageHandlers && webkit.messageHandlers.uppHandler) {
      // iOS
      webkit.messageHandlers.uppHandler.postMessage(JSON.stringify(event));
    } else {
      console.log('No send method detected');
    }
  }
}
