import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { AppConstants } from "../../app.constants";

@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  constructor(private http: HttpClient) {
  }

  getInventoryForProductId(productId: bigint): Observable<any> {
    return this.http.get<any>(`${AppConstants.INVENTORY_API_URL}/inventory`,
      { params: new HttpParams().set('productIds', productId?.toString()) });
  }
}
