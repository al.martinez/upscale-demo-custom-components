import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { AppConstants } from "../../app.constants";

const HTTP_HEADERS = new HttpHeaders()
  .set('content-language', AppConstants.CONTENT_LANGUAGE);

@Injectable({
  providedIn: 'root'
})
export class ProductContentService {

  constructor(private http: HttpClient) {
  }

  updateProductPartial(id: bigint, information: any): Observable<any> {
    return this.http.patch<any>(`${AppConstants.PRODUCT_CONTENT_API_URL}/products/${id}`, information, { headers: HTTP_HEADERS });
  }
}
