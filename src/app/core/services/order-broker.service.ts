import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { AppConstants } from "../../app.constants";

@Injectable({
  providedIn: 'root'
})
export class OrderBrokerService {

  constructor(private http: HttpClient) {
  }

  getOrderForId(orderId: bigint): Observable<any> {
    return this.http.get<any>(`${AppConstants.ORDER_BROKER_API_URL}/external-system/orders/${orderId}`);
  }
}
