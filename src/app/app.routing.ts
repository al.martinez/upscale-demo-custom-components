import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShowOrderInvoiceComponent } from "./components/show-order-invoice/show-order-invoice.component";
import { ShowProductStockComponent } from "./components/show-product-stock/show-product-stock.component";

const routes: Routes = [
  { path: 'show-order-invoice', component: ShowOrderInvoiceComponent },
  { path: 'show-product-stock', component: ShowProductStockComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
