import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ShowOrderInvoiceComponent } from './components/show-order-invoice/show-order-invoice.component';
import { StartupEventsService } from "./core/services/startup-events.service";
import { AppRoutingModule } from "./app.routing";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { AuthInterceptor } from "./core/interceptors/auth.interceptor";
import { ShowProductStockComponent } from './components/show-product-stock/show-product-stock.component';

@NgModule({
  declarations: [
    AppComponent,
    ShowOrderInvoiceComponent,
    ShowProductStockComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }, StartupEventsService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
