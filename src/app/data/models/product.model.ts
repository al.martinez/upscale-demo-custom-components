export class Product {
  private id: bigint;
  private name: string;
  private quantityInStock!: number;

  constructor(id: bigint, name: string) {
    this.id = id;
    this.name = name;
  }

  get getId(): bigint {
    return this.id;
  }

  set setId(value: bigint) {
    this.id = value;
  }

  get getName(): string {
    return this.name;
  }

  set setName(value: string) {
    this.name = value;
  }

  get getQuantityInStock(): number {
    return this.quantityInStock;
  }

  set setQuantityInStock(value: number) {
    this.quantityInStock = value;
  }
}
