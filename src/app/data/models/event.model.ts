export class Event<T> {
  private type: string;
  private data: T;

  constructor(type: string, data: T) {
    this.type = type;
    this.data = data;
  }

  get getType(): string {
    return this.type;
  }

  set setType(value: string) {
    this.type = value;
  }

  get getData(): T {
    return this.data;
  }

  set setData(value: T) {
    this.data = value;
  }
}
