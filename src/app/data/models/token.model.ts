export class Token {
  private accessToken: string;
  private expiresIn: number;

  constructor(accessToken: string, expiresIn: number) {
    this.accessToken = accessToken;
    this.expiresIn = expiresIn;
  }

  get getAccessToken(): string {
    return this.accessToken;
  }

  set setAccessToken(value: string) {
    this.accessToken = value;
  }

  get getExpiresIn(): number {
    return this.expiresIn;
  }

  set setExpiresIn(value: number) {
    this.expiresIn = value;
  }
}
