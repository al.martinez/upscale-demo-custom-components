import { OrderEntry } from "./orderEntry.model";

export class Order {
  private orderId: bigint;
  private entries: OrderEntry[] = [];

  constructor(orderId: bigint) {
    this.orderId = orderId;
  }

  get getOrderId(): bigint {
    return this.orderId;
  }

  set setOrderId(value: bigint) {
    this.orderId = value;
  }

  get getEntries(): OrderEntry[] {
    return this.entries;
  }

  set setEntries(value: OrderEntry[]) {
    this.entries = value;
  }
}
