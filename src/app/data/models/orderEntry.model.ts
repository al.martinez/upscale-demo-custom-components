export class OrderEntry {
  private id: bigint;
  private name: string;
  private quantity: number;

  constructor(id: bigint, name: string, quantity: number) {
    this.id = id;
    this.name = name;
    this.quantity = quantity;
  }

  get getId(): bigint {
    return this.id;
  }

  set setId(value: bigint) {
    this.id = value;
  }

  get getName(): string {
    return this.name;
  }

  set setName(value: string) {
    this.name = value;
  }

  get getQuantity(): number {
    return this.quantity;
  }

  set setQuantity(value: number) {
    this.quantity = value;
  }
}
