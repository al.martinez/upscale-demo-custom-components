import { Component, HostListener, OnInit } from '@angular/core';
import { StartupEventsService } from "../../core/services/startup-events.service";
import { AppConstants } from "../../app.constants";
import { Product } from "../../data/models/product.model";
import { InventoryService } from "../../core/services/inventory.service";
import { ProductContentService } from "../../core/services/product-content.service";

@Component({
  selector: 'app-show-product-stock',
  templateUrl: './show-product-stock.component.html',
  styleUrls: ['./show-product-stock.component.scss']
})
export class ShowProductStockComponent implements OnInit {

  product!: Product;
  requestInProgress: boolean = false;

  constructor(private productService: ProductContentService,
    private inventoryService: InventoryService,
    private upscaleStartupEventService: StartupEventsService) {
  }

  ngOnInit(): void {
    this.upscaleStartupEventService.sendStartupEvents(70);
  }

  @HostListener('window:message', ['$event'])
  private onMessage(event: MessageEvent): void {
    const eventData = event?.data;
    if (eventData?.eventType === AppConstants.PRODUCT_DETAIL_COMPONENT_INIT_EVENT_TYPE) {
      const productDetailInitData = eventData?.keys?.product;
      this.product = new Product(productDetailInitData?.id, productDetailInitData?.name);
    }
  }

  showProductQuantityInStock(): void {
    this.requestInProgress = true;
    this.inventoryService.getInventoryForProductId(this.product?.getId).subscribe(inventory => {
      let quantityInStock: number = 0;
      for (const location of inventory?.content) {
        quantityInStock += location?.onHand;
      }
      this.product.setQuantityInStock = quantityInStock;
      this.requestInProgress = false;
    });
  }

  /* I will keep this commented code in case we need to modify a custom attribute of the product.
  private populateProductQuantityInStock(): void {
    this.inventoryService.getInventoryForProductId(this.product?.getId).subscribe(inventory => {
      let quantityInStock = 0;
      for (const location of inventory?.content) {
        quantityInStock += location?.onHand;
      }
      const customAttributes = { 'customAttributes': { 'quantityInStock': quantityInStock?.toString() } }
      this.productService.updateProductPartial(this.product?.getId, customAttributes).subscribe();
    });
  }*/
}
