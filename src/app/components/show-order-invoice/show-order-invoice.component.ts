import { Component, OnInit } from '@angular/core';
import { StartupEventsService } from "../../core/services/startup-events.service";

@Component({
  selector: 'app-show-invoice',
  templateUrl: './show-order-invoice.component.html',
  styleUrls: ['./show-order-invoice.component.scss']
})
export class ShowOrderInvoiceComponent implements OnInit {

  constructor(private upscaleStartupEventService: StartupEventsService) {
  }

  ngOnInit(): void {
    this.upscaleStartupEventService.sendStartupEvents(70);
  }

  showInvoice(): void {
    window.open('https://upscale-nc.s3.eu-west-3.amazonaws.com/invoice.pdf', '_blank');
  }

  /* I will keep this commented code in case we need to generate the invoice based on the order details.
  private order!: Order;

  constructor(private orderBrokerService: OrderBrokerService, private upscaleStartupEventService: StartupEventsService) {
  }

  @HostListener('window:message', ['$event'])
  private onMessage(event: MessageEvent): void {
    const eventData = event?.data;
    if (eventData?.eventType === AppConstants.ORDER_DETAIL_COMPONENT_INIT_EVENT_TYPE) {
      this.order = new Order(eventData?.keys?.order?.orderId);
    }
  }

  showInvoiceDetails(): void {
    this.orderBrokerService.getOrderForId(this.order?.getOrderId).subscribe(order => {
      this.populateOrderEntries(order);
      alert(`Showing the invoice for order number ${this.order?.getOrderId}: \n ${JSON.stringify(this.order)}`);
    });
  }

  private populateOrderEntries(orderData: any): void {
    for (const orderLine of orderData?.orderLines) {
      this.order.getEntries.push(new OrderEntry(orderLine?.copyProduct?.productId, orderLine?.copyProduct?.name, orderLine?.quantity))
    }
  }*/
}
