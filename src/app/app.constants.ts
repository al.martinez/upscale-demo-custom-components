export class AppConstants {

  public static UPSCALE_API_URL: string = 'https://aspanetconomy-sl-approuter-caas2-sap.cfapps.eu10.hana.ondemand.com';
  public static OAUTH_TOKEN_API_URL: string = `${AppConstants.UPSCALE_API_URL}/oauth2/token`;
  public static ORDER_BROKER_API_URL: string = `${AppConstants.UPSCALE_API_URL}/order-broker`;
  public static INVENTORY_API_URL: string = `${AppConstants.UPSCALE_API_URL}/inventory-service`;
  public static PRODUCT_CONTENT_API_URL: string = `${AppConstants.UPSCALE_API_URL}/product-content`;

  public static ORDER_DETAIL_COMPONENT_INIT_EVENT_TYPE: string = 'order_detail_component_init';
  public static PRODUCT_DETAIL_COMPONENT_INIT_EVENT_TYPE: string = 'product_detail_component_init';

  public static CONTENT_LANGUAGE: string = 'en-US';

  public static CLIENT_ID: string = '071cbf838df58285';
  public static CLIENT_SECRET: string = '7f88f3c69c3cf592319b81647ef03aa3';
  public static GRANT_TYPE: string = 'client_credentials';

}
